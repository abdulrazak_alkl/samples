
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////// initialize street view in step 1

var map;
var myPano;
var panorama;
var houseMarker;
var addLatLng;
var panoOptions;

function load_map_and_street_view_from_address(address) {
  var geocoder = new google.maps.Geocoder();
  geocoder.geocode( { 'address': address}, function(results, status) {
    if (status == google.maps.GeocoderStatus.OK) {
      var gps = results[0].geometry.location;
      create_map_and_streetview(gps.lat(), gps.lng(), 'map-canvas', 'pano');
    }
  });
}


function create_map_and_streetview(lat, lng, map_id, street_view_id) {
  var googlePos = new google.maps.LatLng(lat,lng);

  panorama = new google.maps.StreetViewPanorama(document.getElementById("map-canvas"));
  addLatLng = new google.maps.LatLng(lat,lng);
  var service = new google.maps.StreetViewService();
  service.getPanoramaByLocation(addLatLng, 50, showPanoData);
}

function showPanoData(panoData, status) {
  if (status != google.maps.StreetViewStatus.OK) {
    $('#pano').html('No StreetView Picture Available').attr('style', 'text-align:center;font-weight:bold').show();
    return;
  }
  $('#pano').show();
  var angle = computeAngle(addLatLng, panoData.location.latLng);

  var panoOptions = {
    position: addLatLng,
    addressControl: false,
    linksControl: false,
    panControl: false,
    zoomControlOptions: {
      style: google.maps.ZoomControlStyle.SMALL
    },
    pov: {
      heading: angle,
      pitch: 10,
      zoom: 1
    },
    enableCloseButton: false,
    visible:true
  };
  panorama.setOptions(panoOptions);
}

function computeAngle(endLatLng, startLatLng) {
  var DEGREE_PER_RADIAN = 57.2957795;
  var RADIAN_PER_DEGREE = 0.017453;

  var dlat = endLatLng.lat() - startLatLng.lat();
  var dlng = endLatLng.lng() - startLatLng.lng();
  // We multiply dlng with cos(endLat), since the two points are very closeby,
  // so we assume their cos values are approximately equal.
  var yaw = Math.atan2(dlng * Math.cos(endLatLng.lat() * RADIAN_PER_DEGREE), dlat)
  * DEGREE_PER_RADIAN;
  return wrapAngle(yaw);
}

function wrapAngle(angle) {
  if (angle >= 360) {
    angle -= 360;
  } else if (angle < 0) {
    angle += 360;
  }
  return angle;
}

function initializeStreetView() {
  load_map_and_street_view_from_address(postcodeDataObject.street+" "+postcodeDataObject.houseNumber+", "+postcodeDataObject.city+", Netherlands"); 
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// draw pie chart
google.load("visualization", "1", {packages:["corechart"]});

function drawChart(value, element, savingColor, notSavingColor, text, size){
  var chartData;
  var chartOptions;
  var chart;

  //workaround for IE8/7
  var bgColor = 'transparent';
  if(element == 'final-graph'){
    bgColor = '#00A078';
  }

  chartData = google.visualization.arrayToDataTable([
    ['Type', 'Percentage'],
    ['Potentiële besparing',value],
    ['Totaal verbruik',100-value]
    ]);
  chartOptions = {
    width: size,
    height: size,
    animation:{
      duration: 1000,
      easing: 'out',
    },
    titleTextStyle:{position: 'none'},
    enableInteractivity: false,
    legend: {position: 'none'},
    chartArea:{left:0,top:0,width:"100%",height:"100%"},
    colors: [savingColor, notSavingColor],
    pieSliceBorderColor: 'transparent',
    backgroundColor: bgColor,
    tooltip: { trigger: 'none' },
    pieSliceTextStyle: {color: 'white', fontSize: 17},
    pieSliceText: text
  };

  chart = new google.visualization.PieChart(document.getElementById(element));

  if ((document.documentMode || 100) < 9) {
    document.attachEvent('error',errorHandlerChart);
  }

  chart.draw(chartData, chartOptions);
}

function errorHandlerChart(e){
  google.visualization.errors.removeError(e.id);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// calculation functions

// call the rekentool
function calculateSaving(){
  var o = getValuesForUsageAndSavings(property.type_pr, property.year, property.cost_per_month, property.pakket);
  // console.log(property);
  // console.log(o);
  property.saving.money = Math.round(o.potential_savings);
  property.saving.percentage = Math.round(o.percentage_savings);
  property.estimated_cost_per_month = Math.round(o.average_costs/12);
  property.co2 = o.co2;
}

// get property info (onwership + build year + type) and refresh step 1
function getPropertyInfo(){
  $.ajax({
    contentType: "application/json",
    dataType: "json",
    url: "/jsapi/properties",
    data: JSON.stringify(property),
    type: "POST",
    //async: false,
    success:function(response){
      if(response != null){
        property.state = response.ownership;
        property.year = parseInt(response.buildyear);
        property.type_pr = response.propertytype;
      }else{
        property.state = "Onbekend";
        property.year = "Onbekend";
        property.type_pr = "Onbekend";
      }
      setPropertyInfoInUI();
      RefreshStep1();
    }
  });
}

// get co2 object which contains ranges
function getCo2Data(){
  $.ajax({
    contentType: "application/json",
    dataType: "json",
    url: "/jsapi/co2",
    type:"post",
    success:function(data){ 
      Co2DataObject = data;
    }
  });
}

// edit the property info (onwership + build year + type) and refresh step 1
function editPropertyInfo(){
  getPropertyInfoFromForm();
  setPropertyInfoInUI();
  RefreshStep1();
}

// set edit property form with defualt values
function initPropertyInfoForm(){
  // init ownership
  if(property.state == "koopwoning")
    $('#koopwoning').attr('checked','');
  else if(property.state == "huurwoning")
    $('#huurwoning').attr('checked','');
  else
    $('.pro-state').removeAttr('checked');
  // init the build year
  if(property.year != 'Onbekend')
    $('#buildyear').attr('value',property.year);
  else
    $('#buildyear').attr('value','');
  // init the property type
  $('.option').removeClass('active');
  if(property.type_pr != "Onbekend")
    $(".options .text[data-value="+property.type_pr+"]").parent().addClass('active');
}

// submite the edit property form
function getPropertyInfoFromForm(){
  property.state = $('.property-type input[name=property-type]:checked').val();
  property.year = parseInt($("#buildyear").val());
  property.type_pr = $(".options").find('.active .text').attr('data-value');
}

// update step 1 after edit
function setPropertyInfoInUI(){
  $('.data-property span').text(property.state);
  $('.data-year span').text(property.year);
  $('.data-type span').text(property.type_pr);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// validations functions

// valide the build year and set it
function setBuildYear(){
  buildYear = parseInt($("#buildyear").val());
  if(isNaN(buildYear) || buildYear == undefined || String(buildYear).length < 4 || buildYear > new Date().getFullYear()){
    $("#buildyear").parent().addClass("error");
    return false;
  }else{
    $("#buildyear").parent().removeClass("error");
    //property.year = buildYear;
    return true;
  }
}

// valide the onwership
function validOwnership(){
  var attr1 = $('#koopwoning').attr('checked');
  var attr2 = $('#huurwoning').attr('checked');
  if (typeof attr1 == 'undefined' && typeof attr2 == 'undefined') {
    $('.property-type .errormessage').show();
    return false;
  }
  $('.property-type .errormessage').hide();
  return true;
}

// valide the property type
function validPropertyType(){
  if($('.option').hasClass('active'))
  {
    $('.option').removeClass('error-type');
    $('.options .errormessage').hide();
    return true;
  }else{
    $('.option').addClass('error-type');
    $('.options .errormessage').show();
    return false;  
  }
}

//email validation
function validEmail(e) {
  var filter = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
  return String(e).search (filter) != -1;
}

// phone number validation
function validPhone(p) {
  // if(p == undefined || p == "")
  //   return true;
  //var phoneRe = /\d{10,13}/;
  var digits = p.replace(/\D/g, "");
  if(digits.length >= 10 && digits.length <= 13 && $.isNumeric(digits) && digits != "" )
    return true;
  return false;
}

// name validation
function validName(name){
  if (name.match(/^[a-zA-Z]+$/) && name !="")
    return true;
  return false;
}

//get first element in array
function getFirst(data) {
  for (var prop in data)
    return data[prop];
}

// check if any of the property info unknown
function checkPropertyData() {
  if(property.year == 'Onbekend' || property.state == "Onbekend" || property.type_pr == "Onbekend")
    return false;
  else
    return true;
}

// check if the property not meet criterias of HOOM
function isRejected()
{
  if(property.state == 'huurwoning' || property.year > 1991 || property.type_pr == "Gestapelde-woning" || property.type_pr == "Overig")
    return true;
  return false;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// activate step2 when user set energy cost value
function step2Activation(element){
  if($(element).val() != ""){
    $('.save-view').addClass('active-view');
    property.cost_per_month = parseInt($(element).val());
    checkSavingValue();
  }else{
    property.cost_per_month = null;
    $('.save-view').removeClass('active-view');
  }
}

// handel zero value for energy cost
function checkSavingValue(){
  if (property.saving.money <= 0){
    $('.save-view .none-zero').hide();
    $('.save-view .zero').css({'display': 'table-cell','vertical-align': 'middle'});
  }else{
    $('.save-view .none-zero').css({'display': 'table-cell','vertical-align': 'middle'});
    $('.save-view .zero').hide();
  }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// re-update steps UIs

// refresh step 1
function RefreshStep1(){
  // reset some values
  property.cost_per_month = null;
  property.pakket = 'comfort';
  $( ".energy-form input").val('');
  step2Activation($( ".energy-form input" ));
  checkPakket('comfort');
  // calculate
  calculateSaving();
  property.estimated_cost_per_month = Math.round(property.cost_per_month);
  // set UI
  $('.price span').text(property.saving.money);
  drawChart(property.saving.percentage,'piechart','#4C3687', '#9C89AE', 'percentage', 'auto');
  if(!checkPropertyData()){
    openEditeBox($(".edit-icon"));
  }
}

// refresh step 2
function RefreshStep2(){
  // calculate
  calculateSaving();
  checkSavingValue();
  // set UI
  for(i=0 ; i < Co2DataObject.length; i++)
  {
    if(property.co2 >= parseInt(getFirst(Co2DataObject[i]).field_min_co2_value.und[0].value)
      && property.co2 <= parseInt(getFirst(Co2DataObject[i]).field_max_co2_value.und[0].value))
    {
      $('.calculator-step-2 .save-view .info-panel .info-image img').attr('src',getFirst(Co2DataObject[i]).field_icon.und[0].uri);
      $('.calculator-step-2 .save-view .info-panel .info-text').html(getFirst(Co2DataObject[i]).field_text.und[0].value);
    }
  }

  $('.calculator-step-2 .energy-form .form-text span').text(property.estimated_cost_per_month);
  $('.calculator-step-2 .save-view .save-amount span').text(property.saving.money);
  $('.calculator-step-2 .table-container .one-month span').text(parseInt(property.saving.money/12));
  $('.calculator-step-2 .table-container .one-year span').text(property.saving.money);
  $('.calculator-step-2 .table-container .ten-years span').text(property.saving.money*10);
  $('.calculator-step-2 .table-container .fifteen-years span').text(property.saving.money*15);
}

// refresh step 3
function RefreshStep3(){
  // calculate
  calculateSaving();
  // set UI
  var img;
  var title_text;
  switch(property.pakket)
  {
    case 'comfort':
    img = '/sites/all/themes/hoom_main/images/hoom-home-white.png';
    img1 = '/sites/all/themes/hoom_main/images/hoom-home.png';
    title_text = $('.not-plus').text();
    break;
    case 'comfort plus':
    img = '/sites/all/themes/hoom_main/images/white-house-green-plus.png';
    img1 = '/sites/all/themes/hoom_main/images/green-house-white-plus.png';
    title_text = $('.is-plus').text();
    break;
  }
  $('.calculator-step-4 .info-image img').attr("src", img1);
  $('.calculator-step-3 .result-left-view .info-image img').attr("src",img);
  $('.calculator-step-3 .result-left-view .info-text').text(title_text);
  $('.calculator-step-3 .saved-amount span').text(property.saving.money);
  $('.calculator-step-3 .save-percentage span').text(property.saving.percentage);
  drawChart(property.saving.percentage,'final-graph','#ffffff', '#007457', 'none', 60);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// render steps (steps navigation)

// move to the rejection page
function renderRejection(){
  $("html, body").animate({ scrollTop: 0 });
  $('.rejection').show();
  $('.calculator-step-1').hide();
  $('.calculator-footer-col-right').hide();
}

// move to step 1
function renderStep1(){
  initializeStreetView();
  RefreshStep1();

  $("html, body").animate({ scrollTop: 0 });
  $('.calculator-step-1').show();
  $('.calculator-step-2').hide();
  $('.calculator-step-3').hide();
  $('.calculator-step-4').hide();
  $('.rejection').hide();
  $('.calculator-footer-col-right').show();
  $('.calculator-step-navigation #step-1').addClass('active');
  $('.calculator-step-navigation #step-2').removeClass('active');
  $('.calculator-step-navigation #step-3').removeClass('active');
  $('.calculator-step-navigation #step-1').removeClass('active-done');
  
  $('.calculator-step-navigation .active').off();
  $('.calculator-step-navigation .active').on('click',setNavigation);
  $('.calculator-step-navigation #step-2').off();
  $('.calculator-step-navigation #step-3').off();
}

// move to step 2
function renderStep2(){
  if(isRejected()){
    renderRejection();
  }else {
    // step 2
    RefreshStep2();

    $("html, body").animate({ scrollTop: 0 });
    $('.calculator-step-1').hide();
    $('.calculator-step-2').show();
    $('.calculator-step-3').hide();
    $('.calculator-step-4').hide();
    $('.calculator-step-navigation #step-1').addClass('active active-done');
    $('.calculator-step-navigation #step-2').addClass('active');
    $('.calculator-step-navigation #step-3').removeClass('active');
    $('.calculator-step-navigation #step-2').removeClass('active-done');
    $('.calculator-step-navigation .active').off();
    $('.calculator-step-navigation .active').on('click',setNavigation);
    $('.calculator-step-navigation #step-3').off();
  }
}

// move to step 3
function renderStep3(){
  RefreshStep3();

  $("html, body").animate({ scrollTop: 0 });
  $('.calculator-step-1').hide();
  $('.calculator-step-2').hide();
  $('.calculator-step-3').show();
  $('.calculator-step-4').hide();
  $('.calculator-step-navigation #step-1').addClass('active active-done');
  $('.calculator-step-navigation #step-2').addClass('active active-done');
  $('.calculator-step-navigation #step-3').addClass('active');
  $('.calculator-step-navigation .active').off();
  $('.calculator-step-navigation .active').on('click',setNavigation);

}

// move to success page
function renderStep4(){
  $("html, body").animate({ scrollTop: 0 });
  $('.change-4').text('Heeft u nog vragen');
  $('.calculator-step-navigation #step-1').off();
  $('.calculator-step-navigation #step-2').off();
  $('.calculator-step-navigation #step-3').off();
  $('.calculator-step-1').hide();
  $('.calculator-step-2').hide();
  $('.calculator-step-3').hide();
  $('.calculator-step-4').show();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function setNavigation(){
  switch($(this).attr('id'))
  {
    case 'step-1':
    renderStep1();
    drawChart(property.saving.percentage,'piechart','#4C3687', '#9C89AE', 'percentage', 'auto');
    break;
    case 'step-2':
    renderStep2();
    break;
    case 'step-3':
    renderStep3();
    break;
    default:
    break;
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// open edit property form in popup
var s;
function openEditeBox(source){
  initPropertyInfoForm();

  $.fancybox.open({
    href: "#woning",
    padding: 0,
    width: 534,
    height: 659,
    helpers : {
      overlay : {
        css : { 'background' : 'rgba(255, 255, 255, 0.75)' }
      }
    }
  });
  s = source;
}

$('.edit-property-info').on('click',function(event){
  if( validOwnership() && setBuildYear() && validPropertyType()){
    editPropertyInfo();
    $.fancybox.close( true );
    if($(s).hasClass('button'))
    {
      renderStep2();
    }
  }
});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// pakages
function checkPakket(name){
  var pakketName ='';
  $('.hoom-pakket').each(function(i) {
    pakketName = $(this).find('.hoom-checkbox').attr('data-pakket');
    if(pakketName != name){
      pakketClearActive(this);
    }else{  
      pakketSetActive(this);
      var $imgHolderObject = $(this).find('.info-panel').find('.info-image');
      if($imgHolderObject.hasClass('plus')){
        $imgHolderObject.find('img.default-img-container').attr('src','/sites/all/themes/hoom_main/images/green-house-white-plus.png');
        $imgHolderObject.addClass('isActive');
      }else{
        $('.hoom-pakket').each(function(i) {
          if($(this).find('.info-panel').find('.info-image').hasClass('plus')){
           $(this).find('.plus').find('img.default-img-container').attr('src','/sites/all/themes/hoom_main/images/hoom-home-plus.png');
           $(this).find('.plus').removeClass('isActive');
         }
       });
      }
      property.pakket = pakketName; //$(this).data("pakket");
    }
  });
}

function pakketSetActive(data){
  $(data).find('.hoom-checkbox').find('div').addClass('hoom-checked');
  $(data).find('.info-panel').removeClass('not-active');
}

function pakketClearActive(data){
  $(data).find('.hoom-checkbox').find('div').removeClass('hoom-checked');
  $(data).find('.info-panel').addClass('not-active');
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function cvalidationLoaderShow (data) {
  $(data).find('img').attr('src','sites/all/themes/hoom_main/images/button_loader.gif').css({'margin-top':'-3px','margin-left':'2px'});
 // $(data).find('img').hide();
  //$(data).find('a').text('Start...')
}

function cvalidationLoaderHide (data) {
  $(data).find('img').attr('src','sites/all/themes/hoom_main/images/start-arrow.png').css({'margin-top':'0','margin-left':'10px'});
  //$(data).find('img').show();
  //$(data).find('a').text('Start')
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// news letter subscribution
function subscribeCallback(event){
  var fname = event.data.element.find(".form #tool-fname").val();
  var lname = event.data.element.find(".form #tool-lname").val();  
  var email = event.data.element.find(".form #tool-email").val();
  if(event.data.element.find('.mobile-gender').css('display') == 'none'){
    var gender = event.data.element.find('.form input[name=gender2]:checked').val();
    if(gender == undefined)
      var gender = event.data.element.find('.form input[name=gender4]:checked').val();
  }
  else{
    var gender = event.data.element.find('.form input[name=gender1]:checked').val();
    if(gender == undefined)
      var gender = event.data.element.find('.form input[name=gender3]:checked').val();
  }
  fname = fname.replace(/^\s+/, "").replace(/\s+$/, "").replace(/\s+/, " ");
  lname = lname.replace(/^\s+/, "").replace(/\s+$/, "").replace(/\s+/, " "); 


  if (!validName(fname)) {

    event.data.element.find('.form #tool-fname').removeClass('no-error');
    event.data.element.find('.form #tool-lname').removeClass('no-error');
    event.data.element.find(".row.name").addClass("error");
    event.data.element.find('.form #tool-lname').addClass('no-error');
    event.data.element.find('.errormessage').text('* Vul je voornaam in');


  } else if(!validName(lname)){

    event.data.element.find('.form #tool-fname').removeClass('no-error');
    event.data.element.find('.form #tool-lname').removeClass('no-error');
    event.data.element.find(".row.name").addClass("error");
    event.data.element.find('.form #tool-fname').addClass('no-error');
    event.data.element.find('.errormessage').text('* Vul je achternaam in');

  } else if(!validEmail(email)){


    event.data.element.find(".row.email").addClass("error");


  } else {
    var data = {
      firstname:fname,
      lastname:lname,
      // phone:phone.replace(/\D/g, ""),
      email:email,
      gender:gender,

      // street:$("#street").val(),
      // number:$("#number").val(),
      // extension:$("#extension").val(),
      // postcode:$("#postcode").val(),
      // city:$("#city").val(),
      // type:property.type_pr,
      // year:property.year,
      // property:property.state ,
      // people: property.people,
      // cost_per_month: property.cost_per_month,
      // pakket: property.pakket,
      // saving_per_year: property.saving.money,
      // percentage_saving_per_year: property.saving.percentage,
      // co2: property.co2,

      // electricity: 12 * $( "#current-slider" ).slider("value"),
      // gas: 12 * $( "#gas-slider" ).slider("value"),
      //     // heating
      //     tenants: occupokants,
      //     // result
      source: "newsletter"
      //     gender: gender,
      //     //remark: remark,
      //     // created
      //     // changed

    };
    $.ajax({
      contentType: "application/json",
      dataType: "json",
      url: "/jsapi/lead",
      data: JSON.stringify(data),
      type:"post",
      success:function(){

      }
    });
  }
}