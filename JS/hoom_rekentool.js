/**
 * Create a lookuptable per housetype per buildyear(group)
 */
var average = ['Vrijstaand','Twee-onder-1-kap','Rijwoning','Hoekwoning'];

average['Vrijstaand'] = {
	1964:{
		average: 3669,
		savings:{
			comfort:2103,
		 	comfortplus:2655
		}
	},
	1965:{
		average: 3289,
		savings:{
			comfort:1630,
		 	comfortplus:2182
		}
	},
	1975:{
		average: 2336,
		savings:{
			comfort:673,
		 	comfortplus:1995
		}
	},
	1992:{
		average: 2028,
		savings:{
			comfort:117,
		 	comfortplus:1557
		}
	}
}

average['Twee-onder-1-kap'] = {
	1964:{
		average: 2804,
		savings:{
			comfort:1468,
		 	comfortplus:2020
		}
	},
	1965:{
		average: 2562,
		savings:{
			comfort:1178,
		 	comfortplus:1730
		}
	},
	1975:{
		average: 1827,
		savings:{
			comfort:443,
		 	comfortplus:1577
		}
	},
	1992:{
		average: 1677,
		savings:{
			comfort:131,
		 	comfortplus:1328
		}
	}
}

average['Rijwoning'] = average['Hoekwoning'] = {
	1945:{
		average: 2949,
		savings:{
			comfort:1710,
		 	comfortplus:2261
		}
	},
	1946:{
		average: 2169,
		savings:{
			comfort:1043,
		 	comfortplus:1586
		}
	},
	1965:{
		average: 2052,
		savings:{
			comfort:837,
		 	comfortplus:1383
		}
	},
	1975:{
		average: 1568,
		savings:{
			comfort:364,
		 	comfortplus:1367
		}
	},
	1992:{
		average: 1368,
		savings:{
			comfort:38,
		 	comfortplus:1087
		}
	}
}

function getAverage(housetype, buildyear){
	var table = average[housetype];
	if(table){
		if(buildyear >= 1992)
			return table[1992];
		else if(buildyear >= 1975)
			return table[1975];
		else if(buildyear >= 1965)
			return table[1965];
		else if(table[1946] != null && buildyear >= 1946)
			return table[1946];
		else if(table[1946] == null)
			return table[1964];
		else
			return table[1945];
	}else
		return null;
}

/**
 * MONTHLY_COSTS IS OPTIONAL. IF NOT FILLED OUT IT WILL USE THE AVERAGE VALUES.
 * IF FILLED OUT, IT WILL USE THIS AMOUNT*12(YEAR)
 */
function getValuesForUsageAndSavings(housetype, buildyear, monthly_costs, packet){
	//constants for CO^2 calculation
	var gas_in_co2 = 1.79772;
	var elec_in_co2 = 0.59686;
	//get the item
	var item = getAverage(housetype, buildyear);
	if(item == null){
		return {average_costs:0, potential_savings:0, percentage_savings: 0, percentage_total: 100, co2:0};
	}else{
		var average_costs = costs = item.average;
		var avg_savings = item.savings.comfort;
		var max_savings = item.savings.comfortplus;
		var delta_avg_max = max_savings - avg_savings;
		var percentage_savings = Math.round(delta_avg_max / average_costs * 100);
		var savings_costs = (packet == "comfort plus") ? max_savings : avg_savings;
		
		if(monthly_costs != null){
			costs = monthly_costs*12;
			var savingsratio = savings_costs/average_costs;
			savings_costs = Math.round(costs*savingsratio);
		}

		var co2 = 0;
		if(buildyear < 1975){
			co2 = (costs * 0.85 * gas_in_co2) + (costs * 0.15 * elec_in_co2);
		}else if(buildyear <= 1992){
			co2 = (costs * 0.75 * gas_in_co2) + (costs * 0.25 * elec_in_co2);
		}else if(buildyear > 1992){
			co2 = (costs * 0.65 * gas_in_co2) + (costs * 0.35 * elec_in_co2);
		}
		//console.log(average_costs, avg_savings, max_savings, delta_avg_max, percentage_savings);
		var o = {
			average_costs: average_costs, 
			potential_savings: Math.round(savings_costs), 
			percentage_savings: percentage_savings, 
			percentage_total: 100-percentage_savings, 
			co2:co2
		};

		return o;
	}
}
// console.log( getValuesForUsageAndSavings('Twee-onder-1-kap',1234,506,'comfortplus') )

