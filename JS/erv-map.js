////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ***** create a singal marker in map with info box *******
function createMarker(map,lat,lot,title,media_type,main_image,city_name,body,media_type_img,path){

  // create marker object
  var marker_cor = new google.maps.LatLng(lat,lot);
  var marker = new google.maps.Marker({
    position: marker_cor,
    map: map,
    title: title,
    icon: "/sites/all/themes/hoom_main/images/marker/marker-"+media_type_img+".png"
  });

  // info box of the marker
  var contentString = '<div class="erv-map-block">'+
  '<div class="image"><img src="'+main_image+'"><a href="/'+encodeURI(path)+'"><div class="clipart '+media_type+'"></div></a></div>'+
  '<div class="bottom">'+
  '<div class="title"><h3>'+title+'</h3></div>'+
  '<div class="location">'+city_name+'</div>'+
  ' <div class="text">'+body+'</div>'+
  '<div class="more"><a href="/'+encodeURI(path)+'">Lees meer <img src="/sites/all/themes/hoom_main/images/white-arrow-left.png" /></a></div>'+
  '</div>'+
  '</div>';
  var boxText = document.createElement("div");
  boxText.innerHTML = contentString;
  var myOptions = { content: boxText };
  var ib = new InfoBox(myOptions);

   // ***** hide marker box when you click outside *******
   $('#erv-map').mouseup(function (e){
    ib.close();
  });

   // add onclick event on the marker to open the infobox
   google.maps.event.addListener(marker, 'click', function() {
    ib.open(map, marker);
  });

   marker.setMap(map);
 }
 
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// add campagn markers to map
function get_Campagne_Markers(map){
  $.ajax({
    contentType: "application/json",
    dataType: "json",
    url: "/jsapi/campagne",
    type: "post",
    success:function(response){
     jQuery.each(response, function(i, item) {
      var marker_cor = new google.maps.LatLng(item['lat'],item['lng']);
      var marker = new google.maps.Marker({
        position: marker_cor,
        map: map,
        title: item['campagne_name'],
        icon: "/sites/all/themes/hoom_main/images/marker/filter-campaigns.png"
      });
      marker.setMap(map);
    });
   }
 });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// get list of markers and append them to map
function getAllMarkers(map){

  // get the current city
  var city = "";
  $.ajax({
    contentType: "application/json",
    dataType: "json",
    url: "/jsapi/location",
    type: "post",
    success:function(response){
      city = response;
    }
  });

  // get all experinces items
  $.ajax({
    contentType: "application/json",
    dataType: "json",
    url: "/jsapi/erv",
    data: JSON.stringify(city),
    type: "post",
    success:function(response){

      // create marker for each experince item
      jQuery.each(response, function(i, item) {
        switch (item['field_media_type']['und'][0]['value']) {
          case '1':
          media_type = "story-clipart";
          media_img = "story";
          break;
          case '2':
          media_type = "image-clipart";
          media_img = "gallery";
          break;
          case '3':
          media_type = "video-clipart";
          media_img = "video";
          break;
          default:
          media_type = "story-clipart";
          media_img = "story";
          break;
        }
        createMarker(map,
          item['field_latitude_coordinates']['und'][0]['value'],
          item['field_longitude_coordinates']['und'][0]['value'],
          item['title'],
          media_type,
          item['field_erv_main_image']['und'][0]['url'],
          item['field_erv_city_name']['und'][0]['value'],
          item['body']['und'][0]['value'],
          media_img,
          item['nid']
          );
      });

    }
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function initialize() {

  // get current user location
  var lat = 0;
  var lng = 0;
  $.ajax({
    contentType: "application/json",
    dataType: "json",
    url: "/jsapi/location",
    type: "post",
    success:function(response){

      // create map centered on the current location
      lat = parseFloat(response.latitude);
      lng = parseFloat(response.longitude);
      var mapOptions = {
        center: new google.maps.LatLng(lat,lng),
        zoom: 12
      };
      var map = new google.maps.Map(document.getElementById("erv-map"),mapOptions);
      
      // get all map markers
      getAllMarkers(map);
      get_Campagne_Markers(map);
    }
  });
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$( document ).ready(function() {
  google.maps.event.addDomListener(window, 'load', initialize);
});