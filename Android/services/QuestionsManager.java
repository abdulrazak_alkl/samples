package com.fournet.caring.managers;

import com.fournet.caring.core.Models.Answer;
import com.fournet.caring.core.Models.Checklist;
import com.fournet.caring.core.Models.Question;

import java.util.List;

/**
 * Created by abdulrazakalkl on 5/22/14.
 */
public class QuestionsManager {

    enum Singleton {
        INSTANCE
    }

    static class SingletonHolder {
        static final QuestionsManager INSTANCE = new QuestionsManager();
    }

    public static QuestionsManager getInstance(Checklist checklist) {
        QuestionsManager.checklist = checklist;
        return SingletonHolder.INSTANCE;
    }

    private static Checklist checklist;
    private Question currentQuestion;
    private Integer currentQuestionIndex;
    private Boolean allQuestionsAnswered;


    public Boolean checkAllQuestionsAnswered(){
        for (Question question : checklist.getQuestions()) {
            if ((!question.getIsAnswered()) && (!question.getIsOptional())) {
                setAllQuestionsAnswered(false);
                return false;
            }
        }
        setAllQuestionsAnswered(true);
        return true;
    }

    public static void setQuestionIsOptionalForQuestionID(int questionID){
        Question question = getQuestionByID(questionID);
        if (question != null) {
            question.setIsOptional(true);
            question.setIsAnswered(false);
            if(question.getFollowUpQuestionId() != 0) {
                setQuestionIsOptionalForQuestionID(question.getFollowUpQuestionId());
            } else {
                setQuestionIsOptionalForQuestionID(question.getId() + 1);
            }
            // Clean up the answer if any
            if (question.getAnswers().size() != 0) {
                for (Answer answer : question.getAnswerList()) {
                    answer.setIsAnswer(false);
                    answer.setAnswerInput("");
                    if(answer.getFollowUpQuestionId() != 0) {
                        setQuestionIsOptionalForQuestionID(answer.getFollowUpQuestionId());
                    }
                }
            }
        }
    }

    public static Question getQuestionByID(int questionID){
        for (Question q : checklist.getQuestions()) {
            if(q.getId() == questionID)
                return q;
        }
        return null;
    }

    public static Integer getIndexOfQuestionByID(int questionID){
        List<Question> list = checklist.getQuestions();
        for (int i = 0; i < list.size(); i++) {
            if(list.get(i).getId() == questionID)
                return i;
        }
        return null;
    }

    public Question nextQuestion(){
        if (getCurrentQuestion().getFollowUpQuestionId() == 0)
            setCurrentQuestionIndex(getCurrentQuestionIndex() + 1);
        else
            setCurrentQuestionIndex(getIndexOfQuestionByID(getCurrentQuestion().getFollowUpQuestionId()));
        return getCurrentQuestion();
    }

    public Question getCurrentQuestion() {
        currentQuestion = checklist.getQuestions().get(getCurrentQuestionIndex());
        return currentQuestion;
    }

    public Integer getCurrentQuestionIndex() {
        if (currentQuestionIndex == null)
            currentQuestionIndex = getIndexOfQuestionByID(getChecklist().getStartQuestionId());
        return currentQuestionIndex;
    }

    public void setCurrentQuestionIndex(Integer currentQuestionIndex) {
        this.currentQuestionIndex = currentQuestionIndex;
    }

    public Checklist getChecklist() {
        return checklist;
    }

    public Boolean getAllQuestionsAnswered() {
        return allQuestionsAnswered;
    }

    public void setAllQuestionsAnswered(Boolean allQuestionsAnswered) {
        this.allQuestionsAnswered = allQuestionsAnswered;
    }

    public  void resetCurrentQuestionIndex(){
        currentQuestionIndex = getIndexOfQuestionByID(getChecklist().getStartQuestionId());
    }
}