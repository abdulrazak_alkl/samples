package com.fournet.caring.helpers;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by abdulrazakalkl on 5/21/14.
 */
public class InternalStorage {

    public static void writeObject(Context context, String key, Object object) throws IOException {
        FileOutputStream fos = context.openFileOutput(key, Context.MODE_PRIVATE);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(object);
        oos.close();
        fos.close();
    }

    public static Object readObject(Context context, String key) throws IOException,
            ClassNotFoundException {
        FileInputStream fis = context.openFileInput(key);
        ObjectInputStream ois = new ObjectInputStream(fis);
        Object object = ois.readObject();
        return object;
    }

    public static void removeObject(Context context, String key) throws FileNotFoundException {
        File file = context.getFileStreamPath(key);
        file.delete();
    }

    public static void removeAllObject(Context context) throws FileNotFoundException {
        File file = context.getFilesDir();
        file.delete();
    }

}
