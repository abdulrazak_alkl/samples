package com.fournet.caring.core.DataService;

import android.accounts.AccountsException;
import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.text.format.DateFormat;
import android.util.Log;

import com.fournet.caring.Injector;
import com.fournet.caring.NetworkServiceProvider;
import com.fournet.caring.core.Models.Answer;
import com.fournet.caring.core.Models.ChecklistImage;
import com.fournet.caring.core.Models.ProjectChecklist;
import com.fournet.caring.core.Models.Question;
import com.fournet.caring.core.Models.UserChecklists;
import com.fournet.caring.events.UserChecklistEvent;
import com.fournet.caring.managers.ChecklistManager;
import com.fournet.caring.ui.activities.BootstrapFragmentActivity;
import com.fournet.caring.util.SafeAsyncTask;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;

import javax.inject.Inject;

/**
 * Created by abd on 8/25/14.
 */
public class ChecklistService extends Service {

    @Inject protected NetworkServiceProvider networkServiceProvider;
    @Inject protected Bus eventBus;

    public int branchId;
    LinkedList<Answer> answerQueue;
    ChecklistManager checklistManager;
    Answer currentAnswer;
    int totalQueue;
    int currentQueue;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Injector.inject(this);
        this.eventBus.register(this);
        checklistManager = ChecklistManager.getInstance(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        this.branchId = intent.getIntExtra("branchid",0);
        boolean isRestart = intent.getBooleanExtra("isRestart",false);
        Log.d("caringservice", "servicestarted");

        if(!isRestart) {
            ProjectChecklist projectChecklist = checklistManager.readProjectChecklist(branchId);
            // init answer queue with photos to upload
            answerQueue = new LinkedList<Answer>();
            for (Question question : projectChecklist.getChecklist().getQuestions()) {
                for (Answer answer : question.getAnswerList()) {
                    if (answer.getAnswerPhoto() != null) {
                        answerQueue.add(answer);
                    }
                }
            }
            // init UserChecklist object to submit
            String json = checklistManager.parseChecklistAnswersToJSON(projectChecklist.getChecklist());
            final UserChecklists userChecklists = new UserChecklists();
            userChecklists.setAnswerData(json);
            userChecklists.setBrancheId(projectChecklist.getBrancheId());
            userChecklists.setSiteId(projectChecklist.getSiteId());
            userChecklists.setId(0);
            String date = DateFormat.format("yyyy-MM-ddT00:00:00", new Date()).toString();
            userChecklists.setEnterdate(date);
            userChecklists.setUpdatedate(date);
            userChecklists.setPublish(true);
            userChecklists.setProjectCheckListId(projectChecklist.getId());
            new SafeAsyncTask<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    try {
                        // TODO: get the activity in a better way.
                        networkServiceProvider.getWebService(BootstrapFragmentActivity.activity).updateUserChecklists(userChecklists);
                        return true;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (AccountsException e) {
                        e.printStackTrace();
                    }

                    return false;
                }
            }.execute();
        }
        return START_STICKY;
    }

    @Subscribe
    public void onUserChecklistSubmitted(UserChecklistEvent event){
        if(event.getUserChecklists() == null) {
                    endChecklist();
        } else {
            if (answerQueue.size() == 0) {
                endChecklist();
            } else {
                startUploadingAnswers();
            }
        }
    }

    private void endChecklist(){
        ProjectChecklist projectChecklist = checklistManager.readProjectChecklist(branchId);
        projectChecklist.setSubmitted(true);
        checklistManager.saveProjectChecklist(projectChecklist);
        stopSelf();
    }

    private void startUploadingAnswers(){
        totalQueue = answerQueue.size();
        currentQueue = 1;
        uploadNextImage();
    }

    private void uploadNextImage() {
        if(answerQueue.size() > 0) {
            currentAnswer = answerQueue.getLast();

            Bundle args = new Bundle();
            final ChecklistImage checklistImage = new ChecklistImage();
            // fill the object with data
            ProjectChecklist projectChecklist = checklistManager.readProjectChecklist(branchId);
            checklistImage.setProjectId(projectChecklist.getProjectId());
            checklistImage.setBrancheId(projectChecklist.getBrancheId());
            checklistImage.setImage(currentAnswer.getAnswerPhoto());
            checklistImage.setQuestionId(currentAnswer.getQuestion().getId());

            new SafeAsyncTask<Boolean>() {
                @Override
                public Boolean call() throws Exception {
                    try {
                        // TODO: get the activity in a better way.
                        networkServiceProvider.getWebService(BootstrapFragmentActivity.activity).uploadChecklistImageWithoutEvent(checklistImage);

                        return true;
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (AccountsException e) {
                        e.printStackTrace();
                    }
                    return false;
                }

                @Override
                protected void onSuccess(Boolean aBoolean) throws Exception {
                    doneUploading(currentAnswer);
                }
            }.execute();
        }

    }

    private void doneUploading (Answer answer){
        if(answer != null){
            currentAnswer.setAnswerPhoto(null);
            answerQueue.remove(answer);
        }
        if(answerQueue.size() > 0) {
            currentQueue++;
            uploadNextImage();
        } else {
            endChecklist();
        }
    }

}
