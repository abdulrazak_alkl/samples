package com.fournet.caring.helpers;

import com.fournet.caring.core.Models.Answer;
import com.fournet.caring.core.Models.Question;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by abdulrazakalkl on 5/22/14.
 */
public class QuestionHelper {

    public enum QuestionType{
        QuestionTypeInput (0),
        QuestionTypeMultipleChoice (1),
        QuestionTypeMultipleSelect (2),
        QuestionTypeRetour (3),
        QuestionTypePhoto (4),
        QuestionTypeSignature (5),
        QuestionTypeRemark (6);

        private final int id;
        QuestionType(int id) { this.id = id;}
        public int getValue() { return id; }
    }

    public static Answer answerForQuestion (Question question){
        // Retrieve the answer selected
        Answer answer = null;
        if (question.getAnswerList().size() > 0)
        {
            switch (question.getQuestionType())
            {
                case QuestionTypeInput:
                    answer = question.getAnswerList().get(0);
                    break;
                case QuestionTypeMultipleChoice:
                    List<Answer> list = selectedAnswersForQuestion(question);
                    if(list.size() > 0)
                        answer = list.get(0);
                    break;
            }
        }
        return answer;
    }

    public static List<Answer> selectedAnswersForQuestion (Question question) {
        List<Answer> list = new ArrayList<Answer>();
        for (Answer answer : question.getAnswerList()){
            if(answer.getIsAnswer())
                list.add(answer);
        }
        return list;
    }

    public static QuestionType questionTypeForString (String string){
        if(string.equals("input"))
        {
            return QuestionType.QuestionTypeInput;
        }
        else if(string.equals("multiplechoice"))
        {
            return QuestionType.QuestionTypeMultipleChoice;
        }
        else if(string.equals("multipleselect"))
        {
            return QuestionType.QuestionTypeMultipleSelect;
        }
        else if(string.equals("retour"))
        {
            return QuestionType.QuestionTypeRetour;
        }
        else if (string.equals("photo"))
        {
            return QuestionType.QuestionTypePhoto;
        }
        else if (string.equals("signature"))
        {
            return QuestionType.QuestionTypeSignature;
        }
        else if (string.equals("remark"))
        {
            return QuestionType.QuestionTypeRemark;
        }
        return null;
    }

}
