package com.fournet.caring.ui.Adapters;

import android.app.Activity;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fournet.caring.R;
import com.fournet.caring.core.Models.Product;

import java.util.HashMap;
import java.util.List;

/**
 * Created by abdulrazakalkl on 5/27/14.
 */
public class ProductsAdapter extends CaringGroupListAdapter {
    protected static class ViewHolder {
        public TextView productNumber;
        public TextView productName;
    }

    public ProductsAdapter(Activity act, List<Product> products) {
        activity = act;
        groups = new SparseArray<Group>();

        HashMap<String, Group> map = new HashMap<String, Group>();
        int count = 0;
        for (Product product : products){
            String key = product.getBrand();
            if(!map.containsKey(key)) {
                map.put(key, new Group());
                this.groups.append(count,map.get(key));
                count++;
            }
            map.get(key).children.add(product);
            map.get(key).string = key;
        }
        inflater = act.getLayoutInflater();
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {
            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.product_list_item, null);
            /****** View Holder Object to contain tabitem.xml file elements ******/
            holder = new ViewHolder();
            holder.productName =    (TextView) vi.findViewById(R.id.product_name);
            holder.productNumber =  (TextView)  vi.findViewById(R.id.product_number);
            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        } else
            holder = (ViewHolder)vi.getTag();
        if(groups != null) {
            /***** Get each Model object from Arraylist ********/
            final Product product = (Product) getChild(groupPosition, childPosition);
            /************  Set Model values in Holder elements ***********/
            holder.productNumber.setText(product.getProductAmount() + "");
            holder.productName.setText(product.getProductName());
        }
        return vi;
    }
}
