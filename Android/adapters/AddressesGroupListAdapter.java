package com.fournet.caring.ui.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fournet.caring.R;
import com.fournet.caring.core.Models.Branche;
import com.fournet.caring.ui.activities.BranchOverviewActivity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by abd on 5/12/14.
 */
public class AddressesGroupListAdapter extends CaringGroupListAdapter {

    protected static class ViewHolder {
        public ImageView statusIcon;
        public TextView projectTitle;
        public TextView projectNumber;
        public TextView projectStatus;
    }

    public AddressesGroupListAdapter(Activity act, List<Branche> branches) {
        activity = act;
        groups = new SparseArray<Group>();
        HashMap<Date, Group> map = new HashMap<Date, Group>();
        int count = 0;
        for (Branche branche : branches){
            Date key = branche.getDate();
            if(!map.containsKey(key)) {
                map.put(key, new Group());
                this.groups.append(count,map.get(key));
                count++;
            }
            map.get(key).children.add(branche);
            map.get(key).string = android.text.format.DateFormat.format("dd-MM-yyyy", key).toString();
        }
        inflater = act.getLayoutInflater();
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder;

        if (convertView == null) {
            /****** Inflate tabitem.xml file for each row ( Defined below ) *******/
            vi = inflater.inflate(R.layout.project_list_item, null);

            /****** View Holder Object to contain tabitem.xml file elements ******/
            holder = new ViewHolder();

            holder.statusIcon =    (ImageView) vi.findViewById(R.id.status_icon);
            holder.projectTitle =  (TextView)  vi.findViewById(R.id.project_title);
            holder.projectNumber = (TextView)  vi.findViewById(R.id.project_number);
            holder.projectStatus = (TextView)  vi.findViewById(R.id.project_status);

            /************  Set holder with LayoutInflater ************/
            vi.setTag( holder );
        } else
            holder = (ViewHolder)vi.getTag();

        if(groups != null) {
            /***** Get each Model object from Arraylist ********/
            final Branche branche = (Branche) getChild(groupPosition, childPosition);

            /************  Set Model values in Holder elements ***********/

            switch (branche.getStatus()){
                case 0:
                    holder.statusIcon.setImageResource(R.drawable.status_gray);
                    break;
                case 1:
                case 2:
                    holder.statusIcon.setImageResource(R.drawable.statusiconpending);
                    break;
                case 3:
                    holder.statusIcon.setImageResource(R.drawable.statusiconapproved);
                    break;
                default:
                    holder.statusIcon.setImageResource(R.drawable.status_gray);
            }

            holder.projectTitle.setText(branche.getProject().getTitle());
            holder.projectNumber.setText(branche.getVisitAddressStreet());
            holder.projectStatus.setText(branche.getVisitAddressCity());
            vi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, BranchOverviewActivity.class);
                    Bundle b = new Bundle();
                    b.putParcelable("branch", branche);
                    intent.putExtra("branch", b);
                    activity.startActivity(intent);
                }
            });
        }

        return vi;
    }
}
