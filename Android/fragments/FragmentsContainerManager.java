package com.fournet.caring.ui.Fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * Created by abd on 4/9/14.
 */
public class FragmentsContainerManager {

    public static int lastActiveFragmentIndex = -1;
    protected static AvailabilityFragment availabilityFragment;
    protected static YearPlanningCarouselFragment planningFragment;
    protected static TodayFragment todayFragment;
    protected static DailyReportFragment dailyReportFragment;

    public static void SwitchContainerWithAvailabilityFragment(int container, FragmentActivity activity) {
        if(lastActiveFragmentIndex != 0){
            lastActiveFragmentIndex = 0;
            final FragmentManager fragmentManager = activity.getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(container, getFragment())
                    .commit();
        }

    }

    public static void SwitchContainerWithTodayFragment(int container, FragmentActivity activity) {
        if(lastActiveFragmentIndex != 1){
            lastActiveFragmentIndex = 1;
            final FragmentManager fragmentManager = activity.getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(container, getFragment())
                    .commit();
        }

    }

    public static void SwitchContainerWithPlanningFragment(int container, FragmentActivity activity) {
        if(lastActiveFragmentIndex != 2){
            lastActiveFragmentIndex = 2;
            final FragmentManager fragmentManager = activity.getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(container, getFragment())
                    .commit();
        }

    }

    public static void SwitchContainerWithDailyReportFragment(int container, FragmentActivity activity) {
        if(lastActiveFragmentIndex != 3){
            lastActiveFragmentIndex = 3;
            final FragmentManager fragmentManager = activity.getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(container, getFragment())
                    .commit();
        }
    }

    protected static Fragment getFragment() {
        switch (lastActiveFragmentIndex){
            case 0:
                return getFragment(availabilityFragment);
            case 1:
                return getFragment(todayFragment);
            case 2:
                return getFragment(planningFragment);
            case 3:
                return getFragment(dailyReportFragment);
            default:
                break;
        }
        return null;
    }

    protected static Fragment getFragment(AvailabilityFragment fragment) {
       if(fragment == null)
            availabilityFragment = new AvailabilityFragment();
        return availabilityFragment;
    }

    protected static Fragment getFragment(YearPlanningCarouselFragment fragment) {
       if(fragment == null)
            planningFragment = new YearPlanningCarouselFragment();
        return planningFragment;
    }

    protected static Fragment getFragment(TodayFragment fragment) {
       if(fragment == null)
            todayFragment = new TodayFragment();
        return todayFragment;
    }

    protected static Fragment getFragment(DailyReportFragment fragment) {
       if(fragment == null)
            dailyReportFragment = new DailyReportFragment();
        return dailyReportFragment;
    }

}
