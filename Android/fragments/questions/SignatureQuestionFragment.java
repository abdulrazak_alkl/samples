package com.fournet.caring.ui.Fragments.questions;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.coatedmoose.customviews.SignatureView;
import com.fournet.caring.R;
import com.fournet.caring.core.Models.Answer;
import com.fournet.caring.util.ImageUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import butterknife.InjectView;
import butterknife.Views;

/**
 * Created by abdulrazakalkl on 5/26/14.
 */
public class SignatureQuestionFragment extends QuestionFragment {

    @InjectView(R.id.signatureView1) protected SignatureView signatureView;
    @InjectView(R.id.clear_signature) protected Button clearSignature;
    @InjectView(R.id.checklist_done) protected Button doneButton;
    @InjectView(R.id.signature_name) protected EditText signatureName;
    @InjectView(R.id.signature_image) protected ImageView signatureImageView;

    Boolean isActiveView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signutre_question, container, false);
        Views.inject(this, view);
        super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    protected void setupQuestionSpecificViews() {
        isActiveView = true;
        activity.nextQuestion.setVisibility(View.GONE);
        clearSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clearSignature();
                signatureView.setVisibility(View.VISIBLE);
                signatureImageView.setVisibility(View.GONE);
                isActiveView = true;
            }
        });
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getIntent().removeExtra("current_question");
                if (questionIsValid()){
                    activity.saveChecklist();
                }
                if (activity.checkChecklist())
                    activity.submitChecklist();
            }
        });
    }

    @Override
    protected void fillQuestionAnswer() {
        // Get the data from the checkList instead of the question itself
        Answer currentAnswer = getFirstAnswer();
        if (currentAnswer != null && currentAnswer.getAnswerPhoto() != null) {
            // Show the picture instead
            byte[] imageData = currentAnswer.getAnswerPhoto();
            Bitmap signatureImage = ImageUtils.byteArrayToBitmap(imageData);
            signatureView.setVisibility(View.GONE);
            signatureImageView.setVisibility(View.VISIBLE);
            signatureImageView.setImageBitmap(signatureImage);
            isActiveView = false;
        }
        if (question.getQuestionRemark() != null) {
            signatureName.setText(question.getQuestionRemark());
        }
    }

    @Override
    protected Boolean questionIsValid() {
        if (isActiveView) {
            if (signatureView.getImage() != null && (signatureName.getText().length() > 0)) {
                question.setIsAnswered(true);
                Answer answer = getFirstAnswer();
                answer.setAnswerPhoto(ImageUtils.bitmapToByteArray(signatureView.getImage()));
                question.setQuestionRemark(signatureName.getText().toString());
            }else {
                question.setIsAnswered(false);
            }
        } else {
            if (signatureName.getText().length() > 0)
                question.setIsAnswered(true);
        }
        return question.getIsAnswered();
    }

    public void saveSignature(View view) {
        Bitmap image = signatureView.getImage();
        File sd = Environment.getExternalStorageDirectory();
        File fichero = new File(sd, "signature.jpg");

        try {
            if (sd.canWrite()) {
                fichero.createNewFile();
                OutputStream os = new FileOutputStream(fichero);
                image.compress(Bitmap.CompressFormat.JPEG, 90, os);
                os.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(signatureName.getWindowToken(), 0);
        activity.nextQuestion.setVisibility(View.VISIBLE);
    }
}
