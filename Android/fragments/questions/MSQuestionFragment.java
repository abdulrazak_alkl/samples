package com.fournet.caring.ui.Fragments.questions;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;

import com.fournet.caring.R;
import com.fournet.caring.core.Models.Answer;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.Views;

/**
 * Created by abdulrazakalkl on 5/26/14.
 */
public class MSQuestionFragment extends QuestionFragment {

    @InjectView(R.id.checkbox_container) protected LinearLayout checkbox_container;
    List<CheckBox> checkBoxes = new ArrayList<CheckBox>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ms_question, container, false);
        Views.inject(this, view);
        super.onCreateView(inflater,container,savedInstanceState);
        return view;
    }

    @Override
    protected void setupQuestionSpecificViews() {
        for (Answer a : question.getAnswerList())
        {
            CheckBox checkBox = new CheckBox(getActivity());
            checkBox.setText(a.getLabel());
            checkBox.setButtonDrawable(R.drawable.btn_checkbox_theme);
            checkBox.setTextColor(getResources().getColor(R.color.dark_gray_font));
            checkbox_container.addView(checkBox);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    selectOption(checkbox_container.indexOfChild(buttonView), isChecked);
                }
            });
        }
    }

    @Override
    protected void fillQuestionAnswer() {
        if (question.getIsAnswered()) {
            for (int i = 0; i < question.getAnswerList().size(); i++) {
                if (question.getAnswerList().get(i).getIsAnswer()) {
                    ((CheckBox)checkbox_container.getChildAt(i)).setChecked(true);
                }
            }
        }
    }

    @Override
    protected Boolean questionIsValid() {
        if (question.getAnswerList().size() > 0) {
            for (Answer a : question.getAnswerList()) {
                if (a.getIsAnswer()) {
                    question.setIsAnswered(true);
                    activity.nextQuestion.setEnabled(true);
                    return true;
                }
            }
        }
        question.setIsAnswered(false);
        activity.nextQuestion.setEnabled(false);
        return false;
    }

    private void selectOption(int index, Boolean isChecked){
        // Get index of selected OptionButton
        question.getAnswerList().get(index).setIsAnswer(isChecked);
        questionIsValid();
    }
}
