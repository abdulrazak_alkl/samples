package com.fournet.caring.ui.Fragments.questions;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.fournet.caring.R;
import com.fournet.caring.core.Models.Answer;
import com.fournet.caring.ui.activities.ChecklistCameraActivity;
import com.fournet.caring.util.Constants;
import com.fournet.caring.util.ImageUtils;

import butterknife.InjectView;
import butterknife.Views;

/**
 * Created by abdulrazakalkl on 5/26/14.
 */
public class PhotoQuestionFragment extends QuestionFragment {

    @InjectView(R.id.start_camera) protected Button startCamera;
    @InjectView(R.id.image_preview) protected ImageView imagePreview;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo_question, container, false);
        Views.inject(this, view);
        super.onCreateView(inflater,container,savedInstanceState);
        return view;
    }

    @Override
    protected void setupQuestionSpecificViews() {
        startCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ChecklistCameraActivity.class);
                intent.putExtra("over_camera", question.getQuestionTitle());
                intent.putExtra("actionbar_title", "Camera");
                startActivityForResult(intent, Constants.Notification.CAMERA_CHECKLIST_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void fillQuestionAnswer() {
        if (question.getAnswerList().size() > 0) {
            Answer answer = getFirstAnswer();
            if (answer.getAnswerPhoto() != null) {
                imagePreview.setImageBitmap(ImageUtils.byteArrayToBitmap(answer.getAnswerPhoto()));
                imagePreview.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    protected Boolean questionIsValid() {
        if (question.getAnswerList().size() > 0)
        {
            Answer answer = getFirstAnswer();
            if (answer.getAnswerPhoto() != null)
            {
                question.setIsAnswered(true);
                activity.nextQuestion.setEnabled(true);
                return true;
            }
        }
        question.setIsAnswered(false);
        activity.nextQuestion.setEnabled(false);
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Constants.Notification.CAMERA_CHECKLIST_REQUEST_CODE && resultCode == activity.RESULT_OK) {
            byte[] cameraData = data.getByteArrayExtra("image_data");
            if (cameraData != null) {
                Answer answer = getFirstAnswer();
                answer.setAnswerPhoto(cameraData);
            }
//            activity.getIntent().putExtra("current_question", QuestionsManager.getIndexOfQuestionByID(question.getId()));
        }
    }
}
