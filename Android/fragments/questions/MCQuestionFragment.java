package com.fournet.caring.ui.Fragments.questions;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.fournet.caring.R;
import com.fournet.caring.core.Models.Answer;
import com.fournet.caring.managers.QuestionsManager;

import butterknife.InjectView;
import butterknife.Views;

/**
 * Created by abdulrazakalkl on 5/24/14.
 */
public class MCQuestionFragment extends QuestionFragment {

    @InjectView(R.id.radio_group_question) protected RadioGroup radioGroup;
    int selectedOptionIndex = -1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mc_question, container, false);
        Views.inject(this, view);
        super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    protected void fillQuestionAnswer() {
        if (question.getIsAnswered())
        {
            int i = 0;
            for (Answer answer : question.getAnswerList()){
                if(answer.getIsAnswer()) {
                    radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            selectAnswer(group.indexOfChild(group.findViewById(checkedId)));
                        }
                    });
                    ((RadioButton)radioGroup.getChildAt(i)).setChecked(true);
                    radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            chooseOption(group.indexOfChild(group.findViewById(checkedId)));
                        }
                    });
                    selectedOptionIndex = i;
                }
                i++;
            }
        }
    }

    @Override
    protected Boolean questionIsValid() {
        if (selectedOptionIndex != -1) {
            question.setIsAnswered(true);
            activity.nextQuestion.setEnabled(true);
            return true;
        }
        question.setIsAnswered(false);
        activity.nextQuestion.setEnabled(false);
        return false;
    }

    @Override
    protected void setupQuestionSpecificViews() {
        for (Answer a : question.getAnswerList()) {
            RadioButton radioButton = new RadioButton(getActivity());
            radioButton.setText(a.getLabel());
            radioButton.setButtonDrawable(R.drawable.btn_radio_theme);
            radioButton.setTextColor(getResources().getColor(R.color.dark_gray_font));
            radioGroup.addView(radioButton);
        }

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener(){
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                chooseOption(group.indexOfChild(group.findViewById(checkedId)));
            }
        });
    }

    private void selectAnswer(int index) {
        selectedOptionIndex = index;
        // Unselect all buttons first
        for (Answer a : question.getAnswerList()) {
            a.setIsAnswer(false);
        }
        // set the answer
        Answer chosenAnswer = question.getAnswerList().get(selectedOptionIndex);
        chosenAnswer.setIsAnswer(true);
        questionIsValid();
    }

    private void chooseOption(int index){
        selectedOptionIndex = index;
        // Unselect all buttons first
        for (Answer a : question.getAnswerList()) {
            a.setIsAnswer(false);
        }
        // set the answer
        Answer chosenAnswer = question.getAnswerList().get(selectedOptionIndex);
        chosenAnswer.setIsAnswer(true);
        questionIsValid();
        // Make another question optional
        if (question.getFollowUpQuestionId() == 0) {
            // The chosen answer determines which question get's loaded next.
            for (Answer answer : question.getAnswerList())
                if (answer != chosenAnswer)
                    QuestionsManager.setQuestionIsOptionalForQuestionID(answer.getFollowUpQuestionId());
        }
    }
}
