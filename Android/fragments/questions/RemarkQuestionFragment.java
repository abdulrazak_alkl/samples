package com.fournet.caring.ui.Fragments.questions;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.fournet.caring.R;
import com.fournet.caring.ui.views.CustomEditText;

import butterknife.InjectView;
import butterknife.Views;

/**
 * Created by abdulrazakalkl on 5/26/14.
 */
public class RemarkQuestionFragment extends QuestionFragment {

    @InjectView(R.id.big_editText_question) protected CustomEditText inputRemark;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_remark_question, container, false);
        Views.inject(this, view);
        super.onCreateView(inflater,container,savedInstanceState);
        return view;
    }

    @Override
    protected void setupQuestionSpecificViews() {
        inputRemark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (questionIsValid())
                    question.setQuestionRemark(inputRemark.getText().toString());
            }
        });
        inputRemark.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(inputRemark.getWindowToken(), 0);
                    return true;
                }
                return  false;
            }
        });
    }

    @Override
    protected void fillQuestionAnswer() {
        if (question.getIsAnswered() && question.getQuestionRemark().length() > 0) {
            inputRemark.setText(question.getQuestionRemark());
        }
    }

    @Override
    protected Boolean questionIsValid() {
        if (inputRemark.getText().length() > 0)
            question.setIsAnswered(true);
        else
            question.setIsAnswered(false);
        activity.nextQuestion.setEnabled(question.getIsAnswered());
        return question.getIsAnswered();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(inputRemark.getWindowToken(), 0);
    }
}
