package com.fournet.caring.ui.Fragments.questions;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.fournet.caring.R;
import com.fournet.caring.core.Models.Answer;

import butterknife.InjectView;
import butterknife.Views;

/**
 * Created by abdulrazakalkl on 5/24/14.
 */
public class InputQuestionFragment extends QuestionFragment {

    @InjectView(R.id.editText_question) public EditText inputText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input_question, container, false);
        Views.inject(this, view);
        super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    protected void fillQuestionAnswer() {
        if (question.getIsAnswered()) {
            Answer answer = question.getAnswerList().get(0);
            if (answer != null)
                inputText.setText(answer.getAnswerInput());
        }
    }

    @Override
    protected Boolean questionIsValid() {
        if (inputText.getText().length() > 0) {
            String value = inputText.getText().toString().replace(" ", "");
            if (value.length() > 0) {
                question.setIsAnswered(true);
                Answer answer = getFirstAnswer();
                answer.setAnswerInput(inputText.getText().toString());
                answer.setIsAnswer(true);
                activity.nextQuestion.setEnabled(true);
                return true;
            }
        }
        question.setIsAnswered(false);
        activity.nextQuestion.setEnabled(false);
        return false;
    }

    @Override
    protected void setupQuestionSpecificViews() {
        inputText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                questionIsValid();

            }
        });
        inputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(inputText.getWindowToken(), 0);
                    return true;
                }
                return  false;
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(inputText.getWindowToken(), 0);
    }
}
