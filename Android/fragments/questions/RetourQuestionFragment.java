package com.fournet.caring.ui.Fragments.questions;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.fournet.caring.R;
import com.fournet.caring.core.Models.Answer;
import com.fournet.caring.core.Models.Product;
import com.fournet.caring.core.Models.Question;
import com.fournet.caring.helpers.QuestionHelper;
import com.fournet.caring.managers.QuestionsManager;
import com.fournet.caring.ui.Adapters.ProductsAdapter;
import com.fournet.caring.ui.activities.CameraScannerActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.InjectView;
import butterknife.Views;

import static com.fournet.caring.util.Constants.Notification.*;

/**
 * Created by abdulrazakalkl on 5/26/14.
 */
public class RetourQuestionFragment extends QuestionFragment {

    @InjectView(R.id.product_list) protected FloatingGroupExpandableListView productsList;
    @InjectView(R.id.start_scanner) protected Button startScanning;
    @InjectView(R.id.small_btn_next_question) protected Button next;
    List<Product> products;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_retour_question, container, false);
        Views.inject(this, view);
        super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    protected void setupQuestionSpecificViews() {
        activity.nextQuestion.setVisibility(View.GONE);
        products = activity.getIntent().getParcelableArrayListExtra("scanned_products");
        prepareData();
        ProductsAdapter adapter = new ProductsAdapter(getActivity(), products);
        productsList.setAdapter(new WrapperExpandableListAdapter(adapter));
        productsList.setAdapter(new WrapperExpandableListAdapter(adapter));
        for (int position = 1; position <= adapter.getGroupCount(); position++)
            productsList.expandGroup(position - 1);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.getIntent().removeExtra("current_question");
                activity.moveToNextQuestion();
                activity.nextQuestion.setVisibility(View.VISIBLE);
            }
        });

        startScanning.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, CameraScannerActivity.class);
                intent.putParcelableArrayListExtra("products", (ArrayList<Product>) products);
                startActivityForResult(intent, CAMERA_SCANNER_REQUEST_CODE);
            }
        });
    }

    @Override
    protected void fillQuestionAnswer() {

    }

    @Override
    protected Boolean questionIsValid() {
        question.setProductList(products);
        question.setIsAnswered(true);
        return true;
    }

    private void  prepareData(){
        if (products == null) {
            if (this.question.getDependanceQuestionId() >= 0) {
                products = new ArrayList<Product>();
                Question question = QuestionsManager.getQuestionByID(this.question.getDependanceQuestionId());
                if (question.getIsAnswered()) {
                    Answer answer = QuestionHelper.answerForQuestion(question);
                    for (Product product : this.question.getProductList()) {
                        if (product.getAnswerId() == answer.getId())
                            products.add(product);
                    }
                } else {
                    activity.jumpToQuestion(question);
                }
            } else {
                products = this.question.getProductList();
            }
        }
    }

    @Override
    public void onDestroyView() {
        activity.nextQuestion.setVisibility(View.VISIBLE);
        super.onDestroyView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAMERA_SCANNER_REQUEST_CODE && resultCode == activity.RESULT_OK) {
            activity.getIntent().putParcelableArrayListExtra("scanned_products",data.getParcelableArrayListExtra("scanned_products"));
        }
    }
}
