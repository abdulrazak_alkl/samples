package com.fournet.caring.ui.Fragments.questions;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fournet.caring.core.Models.Answer;
import com.fournet.caring.core.Models.Question;
import com.fournet.caring.ui.Fragments.CaringBaseFragment;
import com.fournet.caring.ui.activities.ChecklistActivity;

import java.util.List;

/**
 * Created by abdulrazakalkl on 5/28/14.
 */
public abstract class QuestionFragment extends CaringBaseFragment {

    protected ChecklistActivity activity;
    protected Question question;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        question = getArguments().getParcelable("question");
        activity = (ChecklistActivity)getActivity();
        setFragmentId(question.getId());
        setupQuestionSpecificViews();
        fillQuestionAnswer();
        questionIsValid();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    protected void nextQuestion(){
        if(questionIsValid()){
            activity.moveToNextQuestion();
        }
    }

    protected Answer getFirstAnswer() {
        if(question != null){
            List<Answer> answers = question.getAnswerList();
            if(answers.size() > 0){
                return answers.get(0);
            } else {
                Answer answer = new Answer();
                answer.setQuestion(question);
                question.getAnswerList().add(answer);
                return answer;
            }
        }
        return null;
    }

    protected abstract void setupQuestionSpecificViews();

    protected abstract void fillQuestionAnswer();

    protected abstract Boolean questionIsValid();

    @Override
    public void onStop() {
        super.onStop();
        if (questionIsValid()){
            activity.saveChecklist();
        }
    }

}
