package com.fournet.caring.ui.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.diegocarloslima.fgelv.lib.FloatingGroupExpandableListView;
import com.diegocarloslima.fgelv.lib.WrapperExpandableListAdapter;
import com.fournet.caring.R;
import com.fournet.caring.core.Models.Branche;
import com.fournet.caring.ui.Adapters.AddressesGroupListAdapter;

import java.util.List;

import butterknife.InjectView;
import butterknife.Views;

/**
 * Created by abd on 5/12/14.
 */
public class AddressesGroupListFragment extends CaringBaseFragment {

    @InjectView(R.id.list_loop) protected FloatingGroupExpandableListView addressesListView;
    @InjectView(R.id.empty_view) protected LinearLayout emptyView;

    private List<Branche> brancheList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.list_looper, container, false);
        Views.inject(this, view);

        brancheList = getArguments().getParcelableArrayList("brancheslist");

        AddressesGroupListAdapter adapter = new AddressesGroupListAdapter(getActivity(), brancheList);
        addressesListView.setAdapter(new WrapperExpandableListAdapter(adapter));
        addressesListView.setAdapter(new WrapperExpandableListAdapter(adapter));
        for (int position = 1; position <= adapter.getGroupCount(); position++)
            addressesListView.expandGroup(position - 1);
        TextView textView = (TextView) emptyView.findViewById(R.id.empty_item_text);
        textView.setText(R.string.NoPlanningFound);
        addressesListView.setEmptyView(emptyView);

        return view;
    }
}
