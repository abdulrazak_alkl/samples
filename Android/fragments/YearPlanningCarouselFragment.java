package com.fournet.caring.ui.Fragments;

import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.github.kevinsawicki.wishlist.ViewUtils;
import com.fournet.caring.R;
import com.fournet.caring.core.DataService.PlanningDataService;
import com.fournet.caring.helpers.Workday;
import com.fournet.caring.events.PlanningEvent;
import com.fournet.caring.ui.Helpers.ThrowableLoader;
import com.squareup.otto.Subscribe;

import java.util.List;

import butterknife.InjectView;

/**
 * Created by abd on 5/8/14.
 */
public class YearPlanningCarouselFragment extends PlanningCarouselFragment {

    @InjectView(R.id.planning_loader) protected RelativeLayout progressBar;
    private static final int LOADER_YEAR_PLANNINGS = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
        progressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {  }
        });

        ViewUtils.setGone(progressBar, false);
        getLoaderManager().initLoader(LOADER_YEAR_PLANNINGS, null, this);
    }

    @Override
    protected void addPagerFragments(List<Fragment> fragments, Bundle addressesArgs, Bundle projectArgs){
        fragments.add(Fragment.instantiate(getActivity(), AddressesGroupListFragment.class.getName(), addressesArgs));
        fragments.add(Fragment.instantiate(getActivity(), ProjectsGroupListFragment.class.getName(),projectArgs));
    }

    @Subscribe
    public void onPlanningReceived(PlanningEvent event){
        if (event.getState() == 3){
            this.brancheArrayList = PlanningDataService.getInstance().getBranches(getActivity());
            this.projectArrayList = PlanningDataService.getInstance().getAllNotConfirmedProjects();
            setTabIndicatorNumber();
            initializeViewPager();
            ViewUtils.setGone(progressBar, true);
        }
    }

    @Override
    public Loader<Void> onCreateLoader(int pId, Bundle pArgs) {
        final Bundle args = pArgs;
        final int id = pId;
        return new ThrowableLoader<Void>(getActivity(), null) {
            @Override
            public Void loadData() throws Exception {
                try {
                    if (getActivity() != null) {
                        switch(id) {
                            case LOADER_YEAR_PLANNINGS:
                                networkServiceProvider.getWebService(getActivity()).getYearPlannings(getToday());
                                break;
                        }
                    }
                } catch (OperationCanceledException e) {
                    Activity activity = getActivity();
                    if (activity != null)
                        activity.finish();
                }
                return null;
            }
        };
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getLoaderManager().destroyLoader(LOADER_YEAR_PLANNINGS);
    }
}
