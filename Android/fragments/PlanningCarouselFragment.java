package com.fournet.caring.ui.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import com.fournet.caring.Injector;
import com.fournet.caring.R;
import com.fournet.caring.core.Models.Branche;
import com.fournet.caring.core.Models.Project;
import com.fournet.caring.ui.Adapters.BootstrapPagerAdapter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import butterknife.InjectView;
import butterknife.Views;

/**
 * Fragment which houses the View pager.
 */
public abstract class PlanningCarouselFragment extends CaringBaseFragment implements TabHost.OnTabChangeListener, ViewPager.OnPageChangeListener {

    @InjectView(android.R.id.tabhost) protected TabHost mTabHost;
    @InjectView(R.id.viewpager) protected ViewPager mViewPager;

    private HashMap<String, TabInfo> mapTabInfo = new HashMap<String, PlanningCarouselFragment.TabInfo>();
    private PagerAdapter mPagerAdapter;
    ArrayList<Project> projectArrayList;
    ArrayList<Branche> brancheArrayList;
    List<Fragment> fragments;

    /**
     * Maintains extrinsic info of a tab's construct
     */
    private class TabInfo {
        private String tag;
        private Class<?> clss;
        private Bundle args;
        private Fragment fragment;
        TabInfo(String tag, Class<?> clazz, Bundle args) {
            this.tag = tag;
            this.clss = clazz;
            this.args = args;
        }
    }

    /**
     * A simple factory that returns dummy views to the Tabhost
     */
    class TabFactory implements TabHost.TabContentFactory {

        private final Context mContext;

        public TabFactory(Context context) {
            mContext = context;
        }

        public View createTabContent(String tag) {
            View v = new View(mContext);
            v.setMinimumWidth(0);
            v.setMinimumHeight(0);
            return v;
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_carousel, container, false);
        Views.inject(this, view);
        Injector.inject(this);

        // initialize the TabHost
        this.initializeTabHost();
        if (savedInstanceState != null) {
            mTabHost.setCurrentTabByTag(savedInstanceState.getString("tab")); //set the tab as per the saved state
        }
        return view;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putString("tab", mTabHost.getCurrentTabTag()); //save the tab selected
        super.onSaveInstanceState(outState);
    }

    protected void initializeViewPager() {
        Bundle projectArgs = new Bundle();
        Bundle addressesArgs = new Bundle();
        projectArgs.putParcelableArrayList("projectslist", projectArrayList);
        addressesArgs.putParcelableArrayList("brancheslist", brancheArrayList);
        fragments = new Vector<Fragment>();

        addPagerFragments(fragments,addressesArgs,projectArgs);

        this.mPagerAdapter = new BootstrapPagerAdapter(getChildFragmentManager(), fragments);
        this.mViewPager.setAdapter(this.mPagerAdapter);
        this.mViewPager.setOnPageChangeListener(this);
        //save last tab
        int pos = this.mTabHost.getCurrentTab();
        this.mViewPager.setCurrentItem(pos);
    }

    // hook
    protected abstract void addPagerFragments(List<Fragment> fragments, Bundle addressesArgs, Bundle projectArgs);

    private void initializeTabHost() {
        mTabHost.setup();
        TabInfo tabInfo = null;
        String  t1 = getString(R.string.pager_title_addresses);
        String t2 = getString(R.string.pager_title_projects);
        TabWidget widget = mTabHost.getTabWidget();

        View tabIndicator1 = widget.inflate(getActivity(), R.layout.custom_tab,null);
        ((TextView) tabIndicator1.findViewById(R.id.title)).setText(t1);

        View tabIndicator2 = widget.inflate(getActivity(), R.layout.custom_tab,null);
        ((TextView) tabIndicator2.findViewById(R.id.title)).setText(t2);

        PlanningCarouselFragment.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec(t1).setIndicator(tabIndicator1), (tabInfo = new TabInfo(t1, AddressesFragment.class, null)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);
        PlanningCarouselFragment.AddTab(this, this.mTabHost, this.mTabHost.newTabSpec(t2).setIndicator(tabIndicator2), (tabInfo = new TabInfo(t2, ProjectListFragment.class, null)));
        this.mapTabInfo.put(tabInfo.tag, tabInfo);
        // Default to first tab
        //this.onTabChanged("Tab1");
        //
        mTabHost.setOnTabChangedListener(this);
    }

    protected void setTabIndicatorNumber(){
        TabWidget widget = mTabHost.getTabWidget();
        View tabIndicator2 = widget.getChildTabViewAt(1);
        if (projectArrayList != null && projectArrayList.size() != 0) {
            tabIndicator2.findViewById(R.id.number).setVisibility(View.VISIBLE);
            ((TextView) tabIndicator2.findViewById(R.id.number)).setText(projectArrayList.size()+"");
        }
    }

    private static void AddTab(PlanningCarouselFragment fragment, TabHost tabHost, TabHost.TabSpec tabSpec, TabInfo tabInfo) {
        // Attach a Tab view factory to the spec
        tabSpec.setContent(fragment.new TabFactory(fragment.getActivity()));
        tabHost.addTab(tabSpec);
    }

    public void onTabChanged(String tag) {
//        TabInfo newTab = this.mapTabInfo.get(tag);
        int pos = this.mTabHost.getCurrentTab();
        this.mViewPager.setCurrentItem(pos);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        this.mTabHost.setCurrentTab(position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onStop() {
        super.onStop();
        if (fragments != null){
            for (Fragment fragment : fragments)
                getChildFragmentManager().beginTransaction().remove(fragment).commitAllowingStateLoss();
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();

        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }
}